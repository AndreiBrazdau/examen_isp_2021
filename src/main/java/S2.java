import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class S2 extends JFrame {
    JButton button;
    JTextArea textArea;
    private String filename="File.txt";
    S2() {
        setTitle("Window");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,300);
        this.setLayout(null);
        setLocationRelativeTo(null);
        setVisible(true);
    }
    public void init(){

        button = new JButton("Button");
        button.setBounds(130,60,80, 20);

        button.addActionListener(new S2.ButtonPress());

        textArea = new JTextArea();
        textArea.setBounds(100,10,150,48);

        add(button);
        add(textArea);
    }
    public static void main(String[] args) {
        new S2();
    }
    class ButtonPress implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                BufferedWriter bw=new BufferedWriter(new FileWriter(filename));
                bw.write(textArea.getText());
                bw.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
